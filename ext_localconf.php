<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Datenbetrieb.' . $_EXTKEY,
	'List',
	array(
		'Seminarcontent' => 'list, show',
		
	),
	// non-cacheable actions
	array(
		'Seminarcontent' => 'list',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Datenbetrieb.' . $_EXTKEY,
	'Search',
	array(
		'Search' => 'form',
		
	),
	// non-cacheable actions
	array(
		'Search' => 'form',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Datenbetrieb.' . $_EXTKEY,
	'Feed',
	array(
		'Feed' => 'list',
	),
	// non-cacheable actions
	array(
		'Feed' => 'list',
	)
);

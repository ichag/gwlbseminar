<?php
namespace Datenbetrieb\Gwlbseminar\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Peter Niederlag <peter.niederlag@datenbetrieb.de>, Datenbetrieb
 *           Max Hellwig <max.hellwig@datenbetrieb.de>, Datenbetrieb
 *           Marcel Grieb <marcel.grieb@datenbetrieb.de>, Datenbetrieb
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Seminarcontents
 */
class SeminarcontentRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	protected $defaultOrderings = array('list_sorting_datetime' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING);

	public function findFromSearch(\Datenbetrieb\Gwlbseminar\Domain\Model\Search $search) {
        $query = $this->createQuery();
        $finalAndConstraints = array();

        // Themen
        if ($search->getSubject()) {
            $constraint = $query->contains('subject', $search->getSubject());
            $finalAndConstraints[] = $query->logicalOr($constraint);
        }

        // Zielgruppen
        if ($search->getTargetgroup()) {
            $constraint = $query->contains('targetgroup', $search->getTargetgroup());
            $finalAndConstraints[] = $query->logicalOr($constraint);
        }

        // Anbieter
        if ($search->getProvider()) {
            $constraint = $query->equals('events.provider', $search->getProvider());
            $finalAndConstraints[] = $query->logicalOr($constraint);
        }

        // Textsuche
        if($search->getText()) {
            $constraints = array();
            $searchFields = array('title', 'content', 'targetgroup_text', 'speaker', 'precondition', 'misc');
            foreach ($searchFields as $field) {
                $constraints[] = $query->like($field, '%' . $search->getText() . '%', false);
            }
            $finalAndConstraints[] = $query->logicalOr($constraints);
        }

        // only apply constraint if at least one constraint is set
        if (count($finalAndConstraints) > 0) {
            $query = $query->matching($query->logicalAnd($finalAndConstraints));
        }

		//$query->setOrderings(array('sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING));

        return $query->execute();
    }
}

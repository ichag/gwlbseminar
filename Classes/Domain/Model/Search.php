<?php

namespace Datenbetrieb\Gwlbseminar\Domain\Model;


class Search extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * @var integer
     */
    protected $subject;

    /**
     * @var integer
     */
    protected $targetgroup;

    /**
     * @var integer
     */
    protected $provider;

    /**
     * @var string
     */
    protected $text;

    /**
     * @return integer
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return integer
     */
    public function getTargetgroup()
    {
        return $this->targetgroup;
    }

    /**
     * @param $targetgroup
     * @return $this
     */
    public function setTargetgroup($targetgroup)
    {
        $this->targetgroup = $targetgroup;
        return $this;
    }

    /**
     * @return integer
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param $provider
     * @return $this
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
}
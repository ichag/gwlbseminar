<?php
namespace Datenbetrieb\Gwlbseminar\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Peter Niederlag <peter.niederlag@datenbetrieb.de>, Datenbetrieb
 *           Max Hellwig <max.hellwig@datenbetrieb.de>, Datenbetrieb
 *           Marcel Grieb <marcel.grieb@datenbetrieb.de>, Datenbetrieb
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Seminarevent
 */
class Seminarevent extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * date
	 * 
	 * @var string
	 * @validate NotEmpty
	 */
	protected $date = '';

	/**
	 * location
	 * 
	 * @var string
	 * @validate NotEmpty
	 */
	protected $location = '';

	/**
	 * attendance
	 * 
	 * @var string
	 */
	protected $attendance = '';

	/**
	 * deadline
	 * 
	 * @var \DateTime
	 * @validate NotEmpty
	 */
	protected $deadline = NULL;

	/**
	 * url
	 * 
	 * @var string
	 */
	protected $url = '';

	/**
	 * misc
	 * 
	 * @var string
	 */
	protected $misc = '';

	/**
	 * provider
	 * 
	 * @var \Datenbetrieb\Gwlbseminar\Domain\Model\Provider
	 */
	protected $provider = NULL;

	/**
	 * Returns the date
	 * 
	 * @return string $date
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Sets the date
	 * 
	 * @param string $date
	 * @return void
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * Returns the location
	 * 
	 * @return string $location
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * Sets the location
	 * 
	 * @param string $location
	 * @return void
	 */
	public function setLocation($location) {
		$this->location = $location;
	}

	/**
	 * Returns the attendance
	 * 
	 * @return string $attendance
	 */
	public function getAttendance() {
		return $this->attendance;
	}

	/**
	 * Sets the attendance
	 * 
	 * @param string $attendance
	 * @return void
	 */
	public function setAttendance($attendance) {
		$this->attendance = $attendance;
	}

	/**
	 * Returns the deadline
	 * 
	 * @return \DateTime $deadline
	 */
	public function getDeadline() {
		return $this->deadline;
	}

	/**
	 * Sets the deadline
	 * 
	 * @param \DateTime $deadline
	 * @return void
	 */
	public function setDeadline(\DateTime $deadline) {
		$this->deadline = $deadline;
	}

	/**
	 * Returns the url
	 * 
	 * @return string $url
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Sets the url
	 * 
	 * @param string $url
	 * @return void
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * Returns the misc
	 * 
	 * @return string $misc
	 */
	public function getMisc() {
		return $this->misc;
	}

	/**
	 * Sets the misc
	 * 
	 * @param string $misc
	 * @return void
	 */
	public function setMisc($misc) {
		$this->misc = $misc;
	}

	/**
	 * Returns the provider
	 * 
	 * @return \Datenbetrieb\Gwlbseminar\Domain\Model\Provider $provider
	 */
	public function getProvider() {
		return $this->provider;
	}

	/**
	 * Sets the provider
	 * 
	 * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Provider $provider
	 * @return void
	 */
	public function setProvider(\Datenbetrieb\Gwlbseminar\Domain\Model\Provider $provider) {
		$this->provider = $provider;
	}

}
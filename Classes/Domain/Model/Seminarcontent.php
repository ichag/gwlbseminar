<?php
namespace Datenbetrieb\Gwlbseminar\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Peter Niederlag <peter.niederlag@datenbetrieb.de>, Datenbetrieb
 *           Max Hellwig <max.hellwig@datenbetrieb.de>, Datenbetrieb
 *           Marcel Grieb <marcel.grieb@datenbetrieb.de>, Datenbetrieb
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Seminarcontent
 */
class Seminarcontent extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 * 
	 * @var string
	 * @validate NotEmpty
	 */
	protected $title = '';

	/**
	 * targetgroupText
	 * 
	 * @var string
	 */
	protected $targetgroupText = '';

	/**
	 * content
	 * 
	 * @var string
	 * @validate NotEmpty
	 */
	protected $content = '';

	/**
	 * speaker
	 * 
	 * @var string
	 * @validate NotEmpty
	 */
	protected $speaker = '';

	/**
	 * precondition
	 * 
	 * @var string
	 */
	protected $precondition = '';

	/**
	 * fee
	 * 
	 * @var string
	 */
	protected $fee = '';

	/**
	 * image
	 * 
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $image = NULL;

	/**
	 * misc
	 * 
	 * @var string
	 */
	protected $misc = '';

	/**
	 * targetgroup
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup>
	 */
	protected $targetgroup = NULL;

	/**
	 * subject
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Subject>
	 */
	protected $subject = NULL;

	/**
	 * events
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent>
	 * @cascade remove
	 */
	protected $events = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 * 
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->targetgroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->subject = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->events = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 * 
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 * 
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the targetgroupText
	 * 
	 * @return string $targetgroupText
	 */
	public function getTargetgroupText() {
		return $this->targetgroupText;
	}

	/**
	 * Sets the targetgroupText
	 * 
	 * @param string $targetgroupText
	 * @return void
	 */
	public function setTargetgroupText($targetgroupText) {
		$this->targetgroupText = $targetgroupText;
	}

	/**
	 * Returns the content
	 * 
	 * @return string $content
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Sets the content
	 * 
	 * @param string $content
	 * @return void
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * Returns the speaker
	 * 
	 * @return string $speaker
	 */
	public function getSpeaker() {
		return $this->speaker;
	}

	/**
	 * Sets the speaker
	 * 
	 * @param string $speaker
	 * @return void
	 */
	public function setSpeaker($speaker) {
		$this->speaker = $speaker;
	}

	/**
	 * Returns the precondition
	 * 
	 * @return string $precondition
	 */
	public function getPrecondition() {
		return $this->precondition;
	}

	/**
	 * Sets the precondition
	 * 
	 * @param string $precondition
	 * @return void
	 */
	public function setPrecondition($precondition) {
		$this->precondition = $precondition;
	}

	/**
	 * Returns the fee
	 * 
	 * @return string $fee
	 */
	public function getFee() {
		return $this->fee;
	}

	/**
	 * Sets the fee
	 * 
	 * @param string $fee
	 * @return void
	 */
	public function setFee($fee) {
		$this->fee = $fee;
	}

	/**
	 * Returns the image
	 * 
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 * 
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 * @return void
	 */
	public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image) {
		$this->image = $image;
	}

	/**
	 * Returns the misc
	 * 
	 * @return string $misc
	 */
	public function getMisc() {
		return $this->misc;
	}

	/**
	 * Sets the misc
	 * 
	 * @param string $misc
	 * @return void
	 */
	public function setMisc($misc) {
		$this->misc = $misc;
	}

	/**
	 * Adds a Targetgroup
	 * 
	 * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup $targetgroup
	 * @return void
	 */
	public function addTargetgroup(\Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup $targetgroup) {
		$this->targetgroup->attach($targetgroup);
	}

	/**
	 * Removes a Targetgroup
	 * 
	 * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup $targetgroupToRemove The Targetgroup to be removed
	 * @return void
	 */
	public function removeTargetgroup(\Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup $targetgroupToRemove) {
		$this->targetgroup->detach($targetgroupToRemove);
	}

	/**
	 * Returns the targetgroup
	 * 
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup> $targetgroup
	 */
	public function getTargetgroup() {
		return $this->targetgroup;
	}

	/**
	 * Sets the targetgroup
	 * 
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup> $targetgroup
	 * @return void
	 */
	public function setTargetgroup(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $targetgroup) {
		$this->targetgroup = $targetgroup;
	}

	/**
	 * Adds a Subject
	 * 
	 * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Subject $subject
	 * @return void
	 */
	public function addSubject(\Datenbetrieb\Gwlbseminar\Domain\Model\Subject $subject) {
		$this->subject->attach($subject);
	}

	/**
	 * Removes a Subject
	 * 
	 * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Subject $subjectToRemove The Subject to be removed
	 * @return void
	 */
	public function removeSubject(\Datenbetrieb\Gwlbseminar\Domain\Model\Subject $subjectToRemove) {
		$this->subject->detach($subjectToRemove);
	}

	/**
	 * Returns the subject
	 * 
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Subject> $subject
	 */
	public function getSubject() {
		return $this->subject;
	}

	/**
	 * Sets the subject
	 * 
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Subject> $subject
	 * @return void
	 */
	public function setSubject(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $subject) {
		$this->subject = $subject;
	}

	/**
	 * Adds a Seminarevent
	 * 
	 * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent $event
	 * @return void
	 */
	public function addEvent(\Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent $event) {
		$this->events->attach($event);
	}

	/**
	 * Removes a Seminarevent
	 * 
	 * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent $eventToRemove The Seminarevent to be removed
	 * @return void
	 */
	public function removeEvent(\Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent $eventToRemove) {
		$this->events->detach($eventToRemove);
	}

	/**
	 * Returns the events
	 * 
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent> $events
	 */
	public function getEvents() {
		return $this->events;
	}

	/**
	 * Sets the events
	 * 
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent> $events
	 * @return void
	 */
	public function setEvents(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $events) {
		$this->events = $events;
	}

}
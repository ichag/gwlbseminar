<?php
namespace Datenbetrieb\Gwlbseminar\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Peter Niederlag <peter.niederlag@datenbetrieb.de>, Datenbetrieb
 *           Max Hellwig <max.hellwig@datenbetrieb.de>, Datenbetrieb
 *           Marcel Grieb <marcel.grieb@datenbetrieb.de>, Datenbetrieb
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Provider
 */
class Provider extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 * 
	 * @var string
	 * @validate NotEmpty
	 */
	protected $title = '';

	/**
	 * longtitle
	 * 
	 * @var string
	 */
	protected $longtitle = '';

	/**
	 * url
	 * 
	 * @var string
	 */
	protected $url = '';

	/**
	 * misc
	 * 
	 * @var string
	 */
	protected $misc = '';

	/**
	 * Returns the title
	 * 
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 * 
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the longtitle
	 * 
	 * @return string $longtitle
	 */
	public function getLongtitle() {
		return $this->longtitle;
	}

	/**
	 * Sets the longtitle
	 * 
	 * @param string $longtitle
	 * @return void
	 */
	public function setLongtitle($longtitle) {
		$this->longtitle = $longtitle;
	}

	/**
	 * Returns the url
	 * 
	 * @return string $url
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Sets the url
	 * 
	 * @param string $url
	 * @return void
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * Returns the misc
	 * 
	 * @return string $misc
	 */
	public function getMisc() {
		return $this->misc;
	}

	/**
	 * Sets the misc
	 * 
	 * @param string $misc
	 * @return void
	 */
	public function setMisc($misc) {
		$this->misc = $misc;
	}

}
<?php

namespace Datenbetrieb\Gwlbseminar\Controller;


class SearchController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /**
     * subjectRepository
     *
     * @var \Datenbetrieb\Gwlbseminar\Domain\Repository\SubjectRepository
     * @inject
     */
    protected $subjectRepository = NULL;

    /**
     * targetgroupRepository
     *
     * @var \Datenbetrieb\Gwlbseminar\Domain\Repository\TargetgroupRepository
     * @inject
     */
    protected $targetgroupRepository = NULL;

    /**
     * providerRepository
     *
     * @var \Datenbetrieb\Gwlbseminar\Domain\Repository\ProviderRepository
     * @inject
     */
    protected $providerRepository = NULL;

    /**
     * action form
     *
     * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Search $search
     * @return void
     */
    public function formAction(\Datenbetrieb\Gwlbseminar\Domain\Model\Search $search = NULL) {
        $subjects = $this->subjectRepository->findAll();
        $targetgroups = $this->targetgroupRepository->findAll();
        $providers = $this->providerRepository->findAll();

        $this->view->assign('search', $search);

        $this->view->assign('subjects', $subjects);
        $this->view->assign('targetgroups', $targetgroups);
        $this->view->assign('providers', $providers);
    }
} 
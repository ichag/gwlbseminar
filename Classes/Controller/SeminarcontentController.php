<?php
namespace Datenbetrieb\Gwlbseminar\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Peter Niederlag <peter.niederlag@datenbetrieb.de>, Datenbetrieb
 *           Max Hellwig <max.hellwig@datenbetrieb.de>, Datenbetrieb
 *           Marcel Grieb <marcel.grieb@datenbetrieb.de>, Datenbetrieb
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * SeminarcontentController
 */
class SeminarcontentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * seminarcontentRepository
	 * 
	 * @var \Datenbetrieb\Gwlbseminar\Domain\Repository\SeminarcontentRepository
	 * @inject
	 */
	protected $seminarcontentRepository = NULL;

	/**
	 * action list
	 * 
	 * @return void
	 */
	public function listAction(\Datenbetrieb\Gwlbseminar\Domain\Model\Search $search = NULL) {
        if($search === NULL) {
            $header = 'Seminarangebote';
            $seminarcontents = $this->seminarcontentRepository->findAll();
        } else {
            $header = 'Suchergebnisse';
            $seminarcontents = $this->seminarcontentRepository->findFromSearch($search);
        }

		$this->view->assign('header', $header);
		$this->view->assign('seminarcontents', $seminarcontents);
	}

	/**
	 * action show
	 * 
	 * @param \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarcontent $seminarcontent
	 * @return void
	 */
	public function showAction(\Datenbetrieb\Gwlbseminar\Domain\Model\Seminarcontent $seminarcontent) {
		$this->view->assign('seminarcontent', $seminarcontent);
	}
}

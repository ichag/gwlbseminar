<?php

namespace Datenbetrieb\Gwlbseminar\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Peter Niederlag <peter.niederlag@datenbetrieb.de>, Datenbetrieb
 *           Max Hellwig <max.hellwig@datenbetrieb.de>, Datenbetrieb
 *           Marcel Grieb <marcel.grieb@datenbetrieb.de>, Datenbetrieb
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Peter Niederlag <peter.niederlag@datenbetrieb.de>
 * @author Max Hellwig <max.hellwig@datenbetrieb.de>
 * @author Marcel Grieb <marcel.grieb@datenbetrieb.de>
 */
class SeminareventTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getDateReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getDate()
		);
	}

	/**
	 * @test
	 */
	public function setDateForStringSetsDate() {
		$this->subject->setDate('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'date',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLocationReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getLocation()
		);
	}

	/**
	 * @test
	 */
	public function setLocationForStringSetsLocation() {
		$this->subject->setLocation('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'location',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAttendanceReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getAttendance()
		);
	}

	/**
	 * @test
	 */
	public function setAttendanceForStringSetsAttendance() {
		$this->subject->setAttendance('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'attendance',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDeadlineReturnsInitialValueForDateTime() {
		$this->assertEquals(
			NULL,
			$this->subject->getDeadline()
		);
	}

	/**
	 * @test
	 */
	public function setDeadlineForDateTimeSetsDeadline() {
		$dateTimeFixture = new \DateTime();
		$this->subject->setDeadline($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'deadline',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getUrlReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getUrl()
		);
	}

	/**
	 * @test
	 */
	public function setUrlForStringSetsUrl() {
		$this->subject->setUrl('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'url',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getMiscReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getMisc()
		);
	}

	/**
	 * @test
	 */
	public function setMiscForStringSetsMisc() {
		$this->subject->setMisc('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'misc',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getProviderReturnsInitialValueForProvider() {
		$this->assertEquals(
			NULL,
			$this->subject->getProvider()
		);
	}

	/**
	 * @test
	 */
	public function setProviderForProviderSetsProvider() {
		$providerFixture = new \Datenbetrieb\Gwlbseminar\Domain\Model\Provider();
		$this->subject->setProvider($providerFixture);

		$this->assertAttributeEquals(
			$providerFixture,
			'provider',
			$this->subject
		);
	}
}

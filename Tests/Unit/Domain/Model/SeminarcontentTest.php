<?php

namespace Datenbetrieb\Gwlbseminar\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Peter Niederlag <peter.niederlag@datenbetrieb.de>, Datenbetrieb
 *           Max Hellwig <max.hellwig@datenbetrieb.de>, Datenbetrieb
 *           Marcel Grieb <marcel.grieb@datenbetrieb.de>, Datenbetrieb
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarcontent.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Peter Niederlag <peter.niederlag@datenbetrieb.de>
 * @author Max Hellwig <max.hellwig@datenbetrieb.de>
 * @author Marcel Grieb <marcel.grieb@datenbetrieb.de>
 */
class SeminarcontentTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarcontent
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarcontent();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() {
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTargetgroupTextReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getTargetgroupText()
		);
	}

	/**
	 * @test
	 */
	public function setTargetgroupTextForStringSetsTargetgroupText() {
		$this->subject->setTargetgroupText('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'targetgroupText',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getContentReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getContent()
		);
	}

	/**
	 * @test
	 */
	public function setContentForStringSetsContent() {
		$this->subject->setContent('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'content',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSpeakerReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getSpeaker()
		);
	}

	/**
	 * @test
	 */
	public function setSpeakerForStringSetsSpeaker() {
		$this->subject->setSpeaker('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'speaker',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPreconditionReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getPrecondition()
		);
	}

	/**
	 * @test
	 */
	public function setPreconditionForStringSetsPrecondition() {
		$this->subject->setPrecondition('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'precondition',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFeeReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getFee()
		);
	}

	/**
	 * @test
	 */
	public function setFeeForStringSetsFee() {
		$this->subject->setFee('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'fee',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForFileReference() {
		$this->assertEquals(
			NULL,
			$this->subject->getImage()
		);
	}

	/**
	 * @test
	 */
	public function setImageForFileReferenceSetsImage() {
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImage($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'image',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getMiscReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getMisc()
		);
	}

	/**
	 * @test
	 */
	public function setMiscForStringSetsMisc() {
		$this->subject->setMisc('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'misc',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTargetgroupReturnsInitialValueForTargetgroup() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getTargetgroup()
		);
	}

	/**
	 * @test
	 */
	public function setTargetgroupForObjectStorageContainingTargetgroupSetsTargetgroup() {
		$targetgroup = new \Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup();
		$objectStorageHoldingExactlyOneTargetgroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneTargetgroup->attach($targetgroup);
		$this->subject->setTargetgroup($objectStorageHoldingExactlyOneTargetgroup);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneTargetgroup,
			'targetgroup',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addTargetgroupToObjectStorageHoldingTargetgroup() {
		$targetgroup = new \Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup();
		$targetgroupObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$targetgroupObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($targetgroup));
		$this->inject($this->subject, 'targetgroup', $targetgroupObjectStorageMock);

		$this->subject->addTargetgroup($targetgroup);
	}

	/**
	 * @test
	 */
	public function removeTargetgroupFromObjectStorageHoldingTargetgroup() {
		$targetgroup = new \Datenbetrieb\Gwlbseminar\Domain\Model\Targetgroup();
		$targetgroupObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$targetgroupObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($targetgroup));
		$this->inject($this->subject, 'targetgroup', $targetgroupObjectStorageMock);

		$this->subject->removeTargetgroup($targetgroup);

	}

	/**
	 * @test
	 */
	public function getSubjectReturnsInitialValueForSubject() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getSubject()
		);
	}

	/**
	 * @test
	 */
	public function setSubjectForObjectStorageContainingSubjectSetsSubject() {
		$subject = new \Datenbetrieb\Gwlbseminar\Domain\Model\Subject();
		$objectStorageHoldingExactlyOneSubject = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneSubject->attach($subject);
		$this->subject->setSubject($objectStorageHoldingExactlyOneSubject);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneSubject,
			'subject',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addSubjectToObjectStorageHoldingSubject() {
		$subject = new \Datenbetrieb\Gwlbseminar\Domain\Model\Subject();
		$subjectObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$subjectObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($subject));
		$this->inject($this->subject, 'subject', $subjectObjectStorageMock);

		$this->subject->addSubject($subject);
	}

	/**
	 * @test
	 */
	public function removeSubjectFromObjectStorageHoldingSubject() {
		$subject = new \Datenbetrieb\Gwlbseminar\Domain\Model\Subject();
		$subjectObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$subjectObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($subject));
		$this->inject($this->subject, 'subject', $subjectObjectStorageMock);

		$this->subject->removeSubject($subject);

	}

	/**
	 * @test
	 */
	public function getEventsReturnsInitialValueForSeminarevent() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getEvents()
		);
	}

	/**
	 * @test
	 */
	public function setEventsForObjectStorageContainingSeminareventSetsEvents() {
		$event = new \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent();
		$objectStorageHoldingExactlyOneEvents = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneEvents->attach($event);
		$this->subject->setEvents($objectStorageHoldingExactlyOneEvents);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneEvents,
			'events',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addEventToObjectStorageHoldingEvents() {
		$event = new \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent();
		$eventsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$eventsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($event));
		$this->inject($this->subject, 'events', $eventsObjectStorageMock);

		$this->subject->addEvent($event);
	}

	/**
	 * @test
	 */
	public function removeEventFromObjectStorageHoldingEvents() {
		$event = new \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarevent();
		$eventsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$eventsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($event));
		$this->inject($this->subject, 'events', $eventsObjectStorageMock);

		$this->subject->removeEvent($event);

	}
}

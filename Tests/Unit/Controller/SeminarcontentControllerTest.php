<?php
namespace Datenbetrieb\Gwlbseminar\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Peter Niederlag <peter.niederlag@datenbetrieb.de>, Datenbetrieb
 *  			Max Hellwig <max.hellwig@datenbetrieb.de>, Datenbetrieb
 *  			Marcel Grieb <marcel.grieb@datenbetrieb.de>, Datenbetrieb
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Datenbetrieb\Gwlbseminar\Controller\SeminarcontentController.
 *
 * @author Peter Niederlag <peter.niederlag@datenbetrieb.de>
 * @author Max Hellwig <max.hellwig@datenbetrieb.de>
 * @author Marcel Grieb <marcel.grieb@datenbetrieb.de>
 */
class SeminarcontentControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Datenbetrieb\Gwlbseminar\Controller\SeminarcontentController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('Datenbetrieb\\Gwlbseminar\\Controller\\SeminarcontentController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllSeminarcontentsFromRepositoryAndAssignsThemToView() {

		$allSeminarcontents = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$seminarcontentRepository = $this->getMock('Datenbetrieb\\Gwlbseminar\\Domain\\Repository\\SeminarcontentRepository', array('findAll'), array(), '', FALSE);
		$seminarcontentRepository->expects($this->once())->method('findAll')->will($this->returnValue($allSeminarcontents));
		$this->inject($this->subject, 'seminarcontentRepository', $seminarcontentRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('seminarcontents', $allSeminarcontents);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenSeminarcontentToView() {
		$seminarcontent = new \Datenbetrieb\Gwlbseminar\Domain\Model\Seminarcontent();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('seminarcontent', $seminarcontent);

		$this->subject->showAction($seminarcontent);
	}
}

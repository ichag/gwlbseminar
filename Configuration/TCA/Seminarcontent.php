<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_gwlbseminar_domain_model_seminarcontent'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_gwlbseminar_domain_model_seminarcontent']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, targetgroup_text, content, speaker, precondition, fee, image, misc, targetgroup, subject, events',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1,
            l10n_parent, l10n_diffsource,
            hidden;;1,
            title,
            targetgroup_text,
            content;;;richtext:rte_transform[mode=ts_links],
            speaker,
            list_sorting_datetime,
            --div--;LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tabs.events,
            events,
            --div--;LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tabs.additional,
            image,
            precondition,
            fee,
            misc;;;richtext:rte_transform[mode=ts_links],
            --div--;LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tabs.search,
            targetgroup,
            subject,
            --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
            starttime,
            endtime'
        ),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_gwlbseminar_domain_model_seminarcontent',
				'foreign_table_where' => 'AND tx_gwlbseminar_domain_model_seminarcontent.pid=###CURRENT_PID### AND tx_gwlbseminar_domain_model_seminarcontent.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'targetgroup_text' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.targetgroup_text',
			'config' => array(
				'type' => 'text',
				'cols' => 25,
				'rows' => 3,
				'eval' => 'trim'
			)
		),
		'content' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.content',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim,required',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'speaker' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.speaker',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'precondition' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.precondition',
			'config' => array(
				'type' => 'text',
				'cols' => 25,
				'rows' => 5,
				'eval' => 'trim'
			)
		),
		'fee' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.fee',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'image' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.image',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'image',
				array('maxitems' => 1),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'misc' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.misc',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'targetgroup' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.targetgroup',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_gwlbseminar_domain_model_targetgroup',
				'MM' => 'tx_gwlbseminar_seminarcontent_targetgroup_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_gwlbseminar_domain_model_targetgroup',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'subject' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.subject',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_gwlbseminar_domain_model_subject',
				'MM' => 'tx_gwlbseminar_seminarcontent_subject_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_gwlbseminar_domain_model_subject',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
							),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'events' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.events',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_gwlbseminar_domain_model_seminarevent',
				'foreign_field' => 'seminarcontent',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 1,
                    'expandSingle' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'list_sorting_datetime' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarcontent.list_sorting_datetime',
			'config' => array(
				'dbType' => 'datetime',
				'type' => 'input',
				'size' => 12,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => '0000-00-00 00:00:00'
			),
		)
	),
);

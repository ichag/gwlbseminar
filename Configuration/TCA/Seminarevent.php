<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_gwlbseminar_domain_model_seminarevent'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_gwlbseminar_domain_model_seminarevent']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, date, location, attendance, deadline, url, misc, provider',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1,
		    l10n_parent,
		    l10n_diffsource,
		    hidden;;1,
		    date;;;richtext:rte_transform[mode=ts_links],
		    location;;;richtext:rte_transform[mode=ts_links],
		    attendance,
		    deadline,
		    url,
		    misc;;;richtext:rte_transform[mode=ts_links],
		    --div--;LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tabs.search,
		    provider,
		    --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
		     starttime,
		     endtime'
        ),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_gwlbseminar_domain_model_seminarevent',
				'foreign_table_where' => 'AND tx_gwlbseminar_domain_model_seminarevent.pid=###CURRENT_PID### AND tx_gwlbseminar_domain_model_seminarevent.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'date' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarevent.date',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim,required',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'location' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarevent.location',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim,required',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'attendance' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarevent.attendance',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'deadline' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarevent.deadline',
			'config' => array(
				'dbType' => 'datetime',
				'type' => 'input',
				'size' => 12,
				'eval' => 'datetime,required',
				'checkbox' => 0,
				'default' => '0000-00-00 00:00:00'
			),
		),
		'url' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarevent.url',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'misc' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarevent.misc',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'provider' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:gwlbseminar/Resources/Private/Language/locallang_db.xlf:tx_gwlbseminar_domain_model_seminarevent.provider',
			'config' => array(
				'type' => 'select',
                'items' => array(
                    array('', '')
                ),
				'foreign_table' => 'tx_gwlbseminar_domain_model_provider',
				'minitems' => 1,
				'maxitems' => 1,
			),
		),
		
		'seminarcontent' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

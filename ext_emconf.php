<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "gwlbseminar"
 *
 * Auto generated by Extension Builder 2014-11-13
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'GWLB Seminarfinder',
	'description' => 'Manage and display seminars',
	'category' => 'plugin',
	'author' => 'Peter Niederlag, Max Hellwig, Marcel Grieb',
	'author_email' => 'peter.niederlag@datenbetrieb.de, max.hellwig@datenbetrieb.de, marcel.grieb@datenbetrieb.de',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);